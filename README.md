# Deck of cards #

## Task ##

Implement class Card that will represent playing card. Implement class Dealer with methods 
`public List<Card> getSortedDeck()` and `public List<Card> getMixedDeck()`.

## Build and Run ##

### Prerequisites ###

- java 8+
- Maven 3.5

### Build project ###

- Clone repository
- Go to the folder
- `mvn clean package`

### Run ###

- `java -jar target/deck_of_cards-1.0-SNAPSHOT-jar-with-dependencies.jar`

package kholod;

import org.junit.Test;

import java.util.List;

import static kholod.Suit.CLUBS;
import static kholod.Value.THREE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class DealerTest {

    @Test
    public void getSortedDeckReturnSortedDeck() {
        Dealer dealer = new Dealer();

        List<Card> cards = dealer.getSortedDeck();

        assertEquals(52, cards.size());
        assertEquals(new Card(CLUBS, THREE), cards.get(2));
    }

    @Test
    public void getMixedDeckReturnMixedDeck() {
        Dealer dealer = new Dealer();

        List<Card> mixedCards1 = dealer.getMixedDeck();
        List<Card> mixedCards2 = dealer.getMixedDeck();
        List<Card> sortedCards = dealer.getSortedDeck();

        assertEquals(52, mixedCards1.size());
        assertEquals(52, mixedCards2.size());

        assertNotEquals(mixedCards1, mixedCards2);
        assertNotEquals(mixedCards1, sortedCards);
        assertNotEquals(mixedCards2, sortedCards);
    }

}
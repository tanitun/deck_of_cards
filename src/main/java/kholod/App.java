package kholod;

public class App {

    public static void main(String[] args) {
        Dealer dealer = new Dealer();
        System.out.println("Sorted: " + dealer.getSortedDeck() + "\n");
        System.out.println("Mixed1: " + dealer.getMixedDeck() + "\n");
        System.out.println("Mixed2: " + dealer.getMixedDeck() + "\n");
        System.out.println("Mixed3: " + dealer.getMixedDeck() + "\n");
    }
}

package kholod;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Dealer {

    private List<Card> cards = new ArrayList<>();

    public Dealer() {
        generate();
        Collections.sort(cards);
    }

    private void generate() {
        for(Suit suit : Suit.values()) {
            for(Value value : Value.values()) {
                cards.add(new Card(suit, value));
            }
        }
    }

    public List<Card> getSortedDeck() {
        return new ArrayList<>(cards);
    }

    public List<Card> getMixedDeck() {
        List<Card> mixedDeck = getSortedDeck();
        Collections.shuffle(mixedDeck);
        return mixedDeck;
    }

}

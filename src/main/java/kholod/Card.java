package kholod;

import java.util.Objects;

public class Card implements Comparable<Card> {

    private Suit suit;
    private Value value;

    public Card(Suit suit, Value value) {
        this.suit = suit;
        this.value = value;
    }

    public Suit getSuit() {
        return suit;
    }

    public Value getValue() {
        return value;
    }

    @Override
    public String toString() {
        return suit + " '" + value + "'";
    }

    @Override
    public int compareTo(Card other) {
        int result = suit.getName().compareTo(other.getSuit().getName());
        if (result == 0) {
          result = value.getOrder() - other.getValue().getOrder();
        }
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Card card = (Card) o;
        return suit == card.suit &&
                value == card.value;
    }

    @Override
    public int hashCode() {
        return Objects.hash(suit, value);
    }
}
